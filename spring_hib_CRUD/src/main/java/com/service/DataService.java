package com.service;

import java.util.List;

import com.model.Employee;

public interface DataService {
	public int insertRow(Employee employee);

	public List<Employee> getList();	

	public Employee getRowById(int id);

	public int updateRow(Employee employee);

	public int deleteRow(int id);
}
