package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.DataDao;
import com.model.Employee;
@Service
public class DataServiceImpl implements DataService{

	@Autowired
	DataDao dao;
	@Override
	public int insertRow(Employee employee) {
		
		return dao.insertRow(employee);
	}

	@Override
	public List<Employee> getList() {
		return dao.getList();
	}

	@Override
	public Employee getRowById(int id) {
		return dao.getRowById(id);
	}

	@Override
	public int updateRow(Employee employee) {
		return dao.updateRow(employee);
	}

	@Override
	public int deleteRow(int id) {
		return dao.deleteRow(id);
	}

}
