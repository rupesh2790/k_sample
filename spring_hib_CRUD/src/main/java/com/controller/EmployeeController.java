package com.controller;
import java.util.List;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Controller;

import com.model.Employee;
import com.service.DataService;

import org.hibernate.dialect.MySQL5Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Controller
public class EmployeeController {
	@Autowired
	DataService service;

	@RequestMapping("form")
	public ModelAndView getForm(@ModelAttribute Employee emp)
	{
       return new ModelAndView("form");
	}
	
	@RequestMapping("register")
	public ModelAndView registerUser(@ModelAttribute Employee emp)
	{
		service.insertRow(emp);
		return new ModelAndView("redirect:list");
	}
	
	@RequestMapping("list")
	public ModelAndView getList()
	{
		List<Employee> emplist=service.getList();
		return new ModelAndView("list","employeeList",emplist);
	}
	
	@RequestMapping("delete")
	public ModelAndView deleteEmp(@RequestParam int id)
	{
		service.deleteRow(id);
		return new ModelAndView("redirect:list");
	}
	
	@RequestMapping("edit")
	public ModelAndView editUser(@RequestParam int id,@ModelAttribute Employee employee)
	{
		Employee emp=service.getRowById(id);
		return new ModelAndView("edit","emp",emp);
	}
	
	@RequestMapping("update")
	public ModelAndView updateEmp(@ModelAttribute Employee employee)
	{
	service.updateRow(employee);
	return new ModelAndView("redirect:list");
	}

}
