package com.dao;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Employee;
@Repository
@Transactional
public class DataDaoImpl implements DataDao{
	@Autowired
	SessionFactory sessionFactory;
	@Override
	public int insertRow(Employee employee) {
		Session session=sessionFactory.openSession();
		//Transaction transaction=session.beginTransaction();
		session.saveOrUpdate(employee);
		//transaction.commit();
		Serializable id=session.getIdentifier(employee);
		session.close();
		return (Integer)id;
	}

	@Override
	public List<Employee> getList() {
		Session session=sessionFactory.openSession();
		//Transaction transaction=session.beginTransaction();
		List<Employee>list= session.createCriteria(Employee.class).list();
        session.close();
		return list;
	}

	@Override
	public Employee getRowById(int id) {
		Session session=sessionFactory.openSession();
		//Transaction transaction=session.beginTransaction();
		Employee emp=(Employee)session.load(Employee.class, id);
		return emp;
	}

	@Override
	public int updateRow(Employee employee) {
		Session session=sessionFactory.openSession();
		//Transaction transaction=session.beginTransaction();
		//Employee emp=(Employee)session.load(Employee.class, employee.getId());
		
		session.merge(employee);
		
		System.out.println(employee.getEmail());
		//transaction.commit();
		Serializable id=session.getIdentifier(employee);
		session.close();
		return (Integer)id;
	}

	@Override
	public int deleteRow(int id) {
		Session session=sessionFactory.openSession();
       // Transaction transaction=session.beginTransaction();
        Employee emp=(Employee)session.load(Employee.class, id);
        session.delete(emp);
       // transaction.commit();
        Serializable x=session.getIdentifier(emp);
        session.close();
		return (Integer)x;
	}

}
