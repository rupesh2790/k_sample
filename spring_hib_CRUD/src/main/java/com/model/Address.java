package com.model;

public class Address {
 private int ad_id;
 
 private String city_name;

public int getAd_id() {
	return ad_id;
}

public void setAd_id(int ad_id) {
	this.ad_id = ad_id;
}

public String getCity_name() {
	return city_name;
}

public void setCity_name(String city_name) {
	this.city_name = city_name;
}
 
 
}
