<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<center>
		<div style="color: teal; font-size: 30px">EDIT</div>
		<c:url var="userRegistration" value="saveUser.html"></c:url>
		<form:form id="registerForm" modelAttribute="employee" method="post" action="update">
		<table width="400px" height="150px">
		<tr>
		<td><form:label path="id">Employee ID</form:label></td>
		<td><form:input path="id" value="${emp.id}" readonly="true"/></td>
		</tr>
		
		<tr>
		<td><form:label path="first_name">FirstName</form:label></td>
		<td><form:input path="first_name" value="${emp.first_name}"/></td>
		</tr>
		
		<tr>
		<td><form:label path="last_name">LaststName</form:label></td>
		<td><form:input path="last_name" value="${emp.last_name}"/></td>
		</tr>
		
		<tr>
		<td><form:label path="email">Email</form:label></td>
		<td><form:input path="email" value="${emp.email}"/></td>
		</tr>
		
		<tr>
		<td><form:label path="number">MobileNo</form:label></td>
		<td><form:input path="number" value="${emp.number}"/></td>
		</tr>
		
		<tr>
		<td></td>
		<td><input type="submit" value="Update"></td>
		</tr>
		</table>
		</form:form>
		
		
	</center>
</body>
</html>